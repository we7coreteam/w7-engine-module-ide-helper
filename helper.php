<?php

/**
 * WeEngine System
 *
 * (c) We7Team 2021 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

if (!function_exists('str_replace_once')) {
    /**
     * 替换一次字符串
     * @param string $search   要替换的字符串
     * @param string $replace  被替换的字符串
     * @param string $subject  主体
     * @return string
     */
    function str_replace_once(string $search, string $replace, string $subject): string
    {
        $pos = strpos($subject, $search);
        if (false === $pos) {
            return $subject;
        }
        return substr_replace($subject, $replace, $pos, strlen($search));
    }
}

if (!function_exists('get_front_string')) {
    /**
     * 取指定字符串前面的字符串,没有指定字符串则返回原字符串
     * @param string $search
     * @param string $subject
     * @return bool|string
     */
    function get_front_string(string $search, string $subject): bool|string
    {
        $strLen = strpos($subject, $search);
        if (!$strLen) {
            $strLen = strlen($subject);
        }
        return substr($subject, 0, $strLen);
    }
}

if (!class_exists('Config')) {
    /**
     * 读取配置
     */
    class Config
    {
        /**
         * 读取系统中app的配置
         *
         * @param string     $name    配置名称
         * @param mixed|null $default 默认值
         * @return mixed
         */
        public static function App(string $name, mixed $default = null): mixed
        {
            return $default;
        }

        /**
         * 读取系统中common的配置
         *
         * @param string     $name    配置名称
         * @param mixed|null $default 默认值
         * @return mixed
         */
        public static function Common(string $name, mixed $default = null): mixed
        {
            return $default;
        }
    }
}

if (!class_exists('AppData')) {
    class AppData
    {
        /**
         * 获取当前访问应用内的路由
         *
         * @return string
         */
        public static function getUri(): string
        {
            return '';
        }

        /**
         * 获取当前应用关联ID
         *
         * @return int
         */
        public static function getLinkId(): int
        {
            return 0;
        }

        /**
         * 获取当前应用ID
         *
         * @return int
         */
        public static function getAppId(): int
        {
            return 0;
        }

        /**
         * 获取当前号码ID
         *
         * @return int
         */
        public static function getAccountId(): int
        {
            return 0;
        }

        /**
         * 获取当前站点url
         *
         * @return string
         */
        public static function getSiteUrl(): string
        {
            return '//' . $_SERVER['HTTP_HOST'];
        }

        /**
         * 获取主框架的Session信息
         *
         * @return array
         */
        public static function getSession(): array
        {
            return [];
        }

        /**
         * 获取Session中小程序用户信息
         *
         * @return array|null
         */
        public static function miniProgram(): array|null
        {
            return [];
        }
    }
}

if (!interface_exists('ModuleInterface')) {
    interface ModuleInterface
    {
        /** 注册 */
        public function register();
        
        /** 启动 */
        public function boot();
    }
}

if (!class_exists('ProviderAbstract')) {
    abstract class ProviderAbstract implements ModuleInterface
    {
        /** @var string 当前应用标识 */
        public string $moduleName;

        /** @var bool 是否为手机应用 */
        public bool $inMobile;
    }
}

/** 模块完整链接 */
!defined('MODULE_URL') && define('MODULE_URL', '');

/** 模块URI地址 */
!defined('MODULE_URI') && define('MODULE_URI', '');

/** 模块静态访问资源URL */
!defined('MODULE_STATIC_URL') && define('MODULE_STATIC_URL', '');

/** 模块文件根目录 */
!defined('MODULE_ROOT') && define('MODULE_ROOT', '');
